/* 
 * @author kubux
 *
 * @date Created on 21 czerwiec 2013, 10:06
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MAXBUF 1100 ///< Maxymalny bufor na informacje

/**
 * struktura z ustawieniami serwera, portu i wiadomosci
 */
struct do_ustawien
{
    char servIP[20];                       //adres ip serwera
    int port;                           //port serwera
    char bufor[MAXBUF];                 //bufor wiadomości
};

/**
 * @brief Wysyłanie blędu na standardowe wyjscie i koniec.
 * 
 * Funkcja wysyła tekst komunikatu na standardowe wyjście i kończy program z błędem
 * 
 * @param wiadomosc - tresc komunikatu
 */

void blad (char *wiadomosc)
{
    fprintf (stderr,wiadomosc); //wysyla komunikat
    exit(EXIT_FAILURE);         //koniec

}

/**
 * @brief Odczyt ustawien z pliku
 * 
 * Funkcja odczytuje z pliku podanego jako argument ustawienia adresu serwera, portu do połączeń, treści wiadomość i kategorii do wysłania
 * 
 * @param ilosc - ilość argumentów do przetworzenia przesłane z main
 * @param param - parametry do przetworzenia przesłane z main
 * @return - zwraca strukturę ustawien do dalszego odczytu
 */
struct do_ustawien ustawienia_z_pliku(int ilosc,char** param)
{
    struct do_ustawien ustawienia;
    FILE *plik;
    char nazwa[50];
    const char *spacja = " ";
    
    strcpy(nazwa,param[2]);
    
    if((plik=fopen(nazwa,"r"))==NULL)
    {
        char text[70];
        sprintf(text,"Blad otwarcia pliku z ustawieniami: %s\n",nazwa);
        blad(text);
    }
    else
    {
        char buf[MAXBUF];
        
        if(fgets(buf,MAXBUF,plik)!=NULL)        //czyta adres serwera
        {
            strcpy(ustawienia.servIP,buf);
        }
        else{close(plik);blad("W pliku nie ma odpowienich ustawien (adres serwera)\n");} //zamyka plik i wysyła komunikat
        
        
        if(fgets(buf,MAXBUF,plik)!=NULL)        //czyta port serwera
        {
            ustawienia.port=atoi(buf);
        }
        else{close(plik);blad("W pliku nie ma odpowienich ustawien (port)\n");}//zamyka plik i wysyła komunikat
        
        
        if(fgets(buf,MAXBUF,plik)!=NULL)        //czyta tytul wiadomosci
        {
            strcpy(ustawienia.bufor,buf);
        }
        else{close(plik);blad("W pliku nie ma odpowienich ustawien (Tytul wiadomosci)\n");}//zamyka plik i wysyła komunikat
        
        if(fgets(buf,MAXBUF,plik)!=NULL)        //czyta tekst wiadomosci
        {
            strcat(ustawienia.bufor,spacja);
            strcat(ustawienia.bufor,buf);
        }
        else{close(plik);blad("W pliku nie ma odpowienich ustawien (Tekst wiadomosci)\n");}//zamyka plik i wysyła komunikat
        
        while (fgets(buf,MAXBUF,plik)!=NULL)    //czyta kategorie
        {
            strcat(ustawienia.bufor,spacja);
            strcat(ustawienia.bufor,buf);
        } 
    }
    close(plik);        //zamyka plik z ustawieniami
    return ustawienia;
}

/**
 * @brief Odczyt ustawien z linii polecen
 * 
 * Funkcja odczytuje z linii polecen ustawienia adresu serwera, portu do połączeń, treści wiadomość i kategorii do wysłania
 * @param ilosc - ilość argumentów do przetworzenia przesłane z main
 * @param param - parametry do przetworzenia przesłane z main
 * @return - zwraca strukturę ustawien do dalszego odczytu
 */

 struct do_ustawien ustawienia_z_linii(int ilosc,char** param)
{
    struct do_ustawien ustawienia;
    strcpy(ustawienia.servIP,param[2]);
    ustawienia.port=atoi(param[3]);
    bzero(ustawienia.bufor,MAXBUF);        //zerowanie bufora
    
    const char *spacja = " ";
    
    strcpy(ustawienia.bufor,param[4]);    
    strcat(ustawienia.bufor,spacja);
    strcat(ustawienia.bufor,param[5]);
    strcat(ustawienia.bufor," Kategoria");
    
    int licznik=6;              //punkt rozpoczęcia pętli
    
    for (licznik; licznik<ilosc; licznik++)
    {
        strcat(ustawienia.bufor,spacja);   
        
        strcat(ustawienia.bufor,param[licznik]);
    }
    return ustawienia;
}



/**, ustawienia
 * @brief Main
 * 
 * Główna pętla programu
 * Pobiera argumenty w wierszu poleceń,
 * otwiera połączenie do wysłania, tworzy wiadomość
 * wysyła dane do serwera
 * 
 * @param argc zawiera ilosc argumentow przsylanych do programu
 * @param argv zawiera tablice argumentów do programu (ip addres,port, tytył, treść, grupa docelowa - max 10)
 * @return 
 */

int main(int argc, char** argv) {

    int sock;                           //sock do komunikacji
    struct sockaddr_in xserver;         //struktura do komunikacji
    
    
    struct do_ustawien ustawienia;
    
    int opcja;                          //przelacznik
    char optstring[]="ts";             //dopuszczale opcje w programie
    char text[MAXBUF];                  //komunikat gdy zle lub brak przelacznikow
    
    strcpy(text,"\nNieprawidłowy przełącznik lub jego brak\n\nPrzoszę użyć przelacznika:\n\n");
    strcat(text,"-t <nazwa pliku z ustawieniami>\n");
    strcat(text,"-s <ip adres> <port> <Tytul wiadomosci> <Tresc wiadomosci> <Grupa docelowa -max 10>\n\n");

    if (argc<2)
        blad(text);
    
    while((opcja=getopt(argc,argv,optstring))!=-1)
        switch (opcja){
            case 't':                                   //przesanie e-maila do wysyki w pliku tekstowym  
                if(argc<3)
                {
                    blad("Prosze podac plik z ustawieniami\n");  
                }
                ustawienia = ustawienia_z_pliku(argc,argv);
                break;
            case 's':                                   //przesanie e-maila do wysyki w linii polecen           
                if (argc < 6)                           //sprawdza ilośc argumentów
                {
                    blad("Prosze podac <ip address> <port> <Tytul wiadomosci> <Tresc> <Grupa docelowa - max 10> \n");
                }
                ustawienia= ustawienia_z_linii(argc,argv);
                break;
            case '?':
            default:

                blad(text);
                break;  
        }
        
        
    sock = socket(PF_INET, SOCK_STREAM, 0);     // tworzy gniazdo
    
    if(sock == -1)
    {
        blad("Nie mozna utworzyc socket!\n");
    }
    
    memset(&xserver,0,sizeof(xserver));
    
    xserver.sin_family = PF_INET;
    xserver.sin_addr.s_addr = inet_addr(ustawienia.servIP);
    xserver.sin_port = htons(ustawienia.port);
    
    if (connect (sock, (const struct sockaddr *) &xserver, sizeof(xserver)) !=0)        //łączenie z serwerem
    {
        blad("Nie można połączyć z serwerem!\n");
    }
    
    
    
    if (write(sock,ustawienia.bufor,MAXBUF)!=MAXBUF)       //wysyłanie wiadomości do serwera
    {
        perror("Write() - przeslano zla ilczbe bajtow");
        close(sock);
    }
    else
    {
        printf("\nPrzesano e-maila do wysyki\n");
        
        
        
        int n=read(sock,ustawienia.bufor,MAXBUF);
        if (n>0)
            printf("%s",ustawienia.bufor);
        n=0;
        do
        {
            n=read(sock,ustawienia.bufor,MAXBUF);
            
        }while(n<201);
        
       
            printf("%s\n",ustawienia.bufor);
        close(sock);
    }
    
    return (EXIT_SUCCESS);
}

