/**
 * @author Wachowicz 12204
 *
 * @date Created on 21 czerwiec 2013, 10:06
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>

#define MAX_BUFOR 1300  ///< wielkosc bufora na wiadomosc
#define MAX_MAIL 40     ///< wielkosc pojedynczego maila

int sock,sock1;         //socket serwera i nasłuchu

/**
 *struktura e-maila 
 */

struct struktura_emaila
{
    char Temat[50];             ///< Temat e-maila do wysylki
    char Tresc[800];            ///< Tresc e-maila do wysylki
    char Kategoria[9][50];      ///< Kategorie na ktore ma zostac wyslany e-mail
    int ilosc;                  ///< Ilosc kategorii na ktore ma byc wyslany e-mail
    
};
void log_do_pliku(char *wiadomosc);

/**
 * @brief Wysyłanie blędu na standardowe wyjscie i koniec.
 * 
 * Funkcja wysyła tekst komunikatu na standardowe wyjście i kończy program z błędem
 * 
 * @param wiadomosc - tresc komunikatu
 */

void blad (char *wiadomosc)
{
    fprintf (stderr,wiadomosc); //wysyla komunikat
    log_do_pliku(wiadomosc);
    write(sock1,wiadomosc,MAX_BUFOR);
    close(sock);
    close(sock1);
    exit(EXIT_FAILURE);         //koniec

}

/**
 * @brief Aktualny czas
 * 
 * Funkcja zwraca aktualny czas lokalny w formacie HH:MM:SS
 * 
 * @return tablica char zawierajca czas
 */
char* czas()
{
    time_t te;                  // zmienna do aktualnego czasu
    struct tm *timeinfo;        // zmienna do lokalnego czasu
    char bufor[10];             // zmienna na czas sformatowany
    
    time (&te);                 // pobiera aktualny czas
   
    timeinfo = localtime(&te);  // formaruje na czas lokalny
    
    strftime(bufor,100,"%T",timeinfo);  // formatowanie czasu i wstawienie do bufora
    
    return bufor;               
}

/**
 * @brief Aktualna data
 * 
 * Funkcja zwraca aktualną datę w formacie dd_mm_yy
 * 
 * @return tablica char zawierajca date
 */
char* data()
{
    time_t te;                  // zmienna do aktualnego czasu
    struct tm *timeinfo;        // zmienna do lokalnego czasu
    char bufor[10];             // zmienna na czas sformatowany
    
    time (&te);                 // pobiera aktualny czas
   
    timeinfo = localtime(&te);  // formaruje na czas lokalny
    
    strftime(bufor,10,"%m_%d_%y",timeinfo);     // formatowanie daty i wstawienie do bufora
    
    return bufor;
}

/**
 * @brief Zapis logu
 * 
 * Funkcja zapisuje logi do pliku 
 * Plik w formacie "data".txt.
 * Jesli plik nie istnieje zostanie utworzony.
 * Jeśli jest log zostaje dopisany na końcu
 * 
 * @param wiadomosc - wiadomosc do preslania
 */

void log_do_pliku(char *wiadomosc)
{
    FILE *plik1;                                        // zmienna na plik
    char nazwa[20];                                     // zmienna na nazwe pliku
    
    sprintf(nazwa,"%s.txt",data());                     // formatowanie nazwy pliku jako date z rozszezeniem 'txt' i wstawienie do zmiennej nazwa
    
    if((plik1=fopen(nazwa,"ab+"))==NULL)                // jesli otwarcie pliku sie niepowiodlo
    {
        printf("Blad otwarcia pliku %s\n",nazwa);       // komunikat o bledzie wyswietlony na konsoli
    }                                           
    else                                                // w p.p
    {
        fputs(wiadomosc,plik1);                         // umieszczenie komunikatu w pliku logu
        fclose(plik1);                                  // zamkniecie pliku
    }
    
}
/**
 * @brief Wysłanie e-maila
 * 
 * Funkcja składa komende do wysłania maila za pośrednictwem z konta gmail,
 *  oraz zapisuje do logu fakt wysłania wraz z czasemi kategorią
 * 
 * @param temat -Temat wiadomosci
 * @param tresc - Tresc wiadomosci
 * @param mail - Adres e-mail do wyslania
 * @param kategoria - Kategoria na którą wysyłany jest e-mail
 */
void sendemail(const char* temat, const char* tresc,const char* mail,char *kategoria)
{
   char c_mail[500];                                    // tablica char na e-maila
    
    sprintf(c_mail,"echo \"%s\" | mail -s \"%s\" %s",tresc,temat,mail);                 // laczenie e-maila w calosc 
    
    char text[300];                                     // tablica czar na log 
   
    sprintf(text,"Wyslano na adres: %30s z grupy: %10s o godzinie: %10s\n",mail,kategoria,czas());      // laczenie loga w calosc 
    
    log_do_pliku(text);                 // wyslanie komunikatu do loga 
    
    //system(c_mail);                   // wysylanie e-maila 
}

/**
 * @brief Odczyt e-maili
 * 
 * Funkcja odczytuje adresy e-mailna które mają być wysyłane wiadomości.
 * Adresy pobiera z plików tekstowych, których nazwy są takie same jak kategorie.
 * 
 * @param mail - struktura zawierająca wiadomość
 */
void czytanie_mail_z_pliku(struct struktura_emaila *mail)
{
    FILE *plik;
    
    int i=0;                                    // zmienna licznika
    char nazwa[50];
    
    for (i;i<mail->ilosc;i++)                   // petla przechodzacz przez wszystkie kategorie 
    {
        bzero(nazwa,50);                        // czyszczenie bufora nazwy pliku 
        
        strcpy(nazwa,mail->Kategoria[i]);       //tworzenie nazwy pliku
        strcat(nazwa,".txt");
        
        
        if((plik=fopen(nazwa,"r"))==NULL)       //próba otwaria pliku
        {
            char text[80];
            sprintf(text,"Blad otwarcia pliku %s\n",nazwa);       // komunikat o bledzie wyswietlony na konsoli
            log_do_pliku(text);
            printf("%s",text);
        }
        else                                            //powodzenie otwarcia
        {
            char buf[MAX_MAIL];
 
            while (fgets(buf,MAX_MAIL,plik)!=NULL)      //dopóki są adresy w pliku
            {
                sendemail(mail->Temat,mail->Tresc,buf,mail->Kategoria[i]);      //wyslii e-mail
            }
            
            close(plik);        //zamknij plik
        }
    }
} 
/**
 * @brief Rozdzielenie informacji od klienta
 * 
 * Funkcja rozdziela ciąg znaków przesłanych od klienta na poszczególne składowe struktury wiadomości
 * 
 * @param bufor - Zawiera ciąg znaków od klienta
 */
void dekodowanie_informacji(char *bufor)
{
    struct struktura_emaila mail;
    int n=0;
    char *slowo; // zmienna pomocnicza no poszczegolne slowo z bufora 
    
    strcpy(mail.Temat,strtok(bufor," "));      // kopiuje z bufora temat wiadomości 
    
    strcpy(mail.Tresc,strtok(NULL," "));       // kopiuje z bufora tresc wiadomosci 
    
    if(slowo=strtok(NULL," "))                  //kopiowanie z bufora tresci wiadomosci dopoki nie nastapi znaczki Kategoria
        if(slowo!="Kategoria")
        {
            do
            {
                strcat(mail.Tresc,slowo);
                strcat(mail.Tresc," ");
                slowo=strtok(NULL," ");
            }
            while(strcmp(slowo,"Kategoria\n")&& strcmp(slowo,"Kategoria"));
            
        }
            
    
    
        
    // funkcja rozbija kategorie z bufora na postawie spacji i umieszcza w tablicy kategorii 
    if(slowo=strtok(NULL," ")){
        mail.ilosc=0;
        do
        {
            strcpy(mail.Kategoria[mail.ilosc],slowo);
            mail.ilosc++;
        }
        while (slowo=strtok(NULL," "));
    }
    
    czytanie_mail_z_pliku(&mail);
    
}

/**
 * @brief Main
 * 
 * Główna pętla programu
 * Tworzy połaczenie i nasłuchuje komunikatów.
 * Gdy przyjdzie wiadomość od klienta przesyła do obróbki i wysłania
 * 
 * @param argc ilosc argumentow
 * @param argv tablica argumentow
 * @return 
 */

int main(int argc, char** argv) {
    
    
    int addrlen;                        
    struct sockaddr_in xserver,xclient; //struktura do socket
    int returnStatus;
   
    
    char bufor[MAX_BUFOR];              //bufor na wiadomośc od klienta
        
    int port;                           // port do polaczen 
    
                                        // Utworz gniazdo 
    
    if(argc<2)
    {
        fprintf(stderr,"Uzycie: %s <port>\n",argv[0]);
        exit(EXIT_FAILURE);
    }
           
    
    
    
    port = atoi(argv[1]);
    
    sock=socket(PF_INET, SOCK_STREAM, 0);
    
    if (sock ==-1)                      
    {
        blad("Could not create socket!\n");
        exit(1);
    }
    
    // Przypisz adres gniazdu 
    xserver.sin_family = AF_INET;
    xserver.sin_addr.s_addr=htonl(INADDR_ANY);
    xserver.sin_port = htons(port);
    
    returnStatus = bind(sock, (struct sockaddr*)&xserver, sizeof(xserver));
    
    if (returnStatus == -1)
    {
        blad("Could not bind to socket!\n");
        close(sock);
    }
    
    returnStatus = listen(sock,5);
    
    if (returnStatus == -1)
    {
        blad( "Could not listen to socket!\n");
        
    }
    
    while(1)
    {
        addrlen=sizeof(xclient);
        sock1 = accept(sock,(struct sockaddr*)&xclient,&addrlen); //próba akceptacji połaczenia z klientem
        
        if (sock1 == -1)
        {
            fprintf(stderr, "Could not accept connection!\n");
            exit(1);
        }
                
         bzero(bufor,MAX_BUFOR);
         
        int n = read(sock1,bufor,MAX_BUFOR);    //czyta z bufora
        
        if (n>0)                                //jeśli czytanie się powiodło
        {
            bufor[n-1]='\0';                    //dodaje znak końca linii
            
            write(sock1,"Odebrano informacje\nTrwa wysylka e-maili\n",MAX_BUFOR);
            
            dekodowanie_informacji(bufor);      //przesyła wiadomośc do obróbki
            
            write(sock1,"\nWysylka zakonczona\n");
          
            close(sock1);                       //zamyka połaczenie
        }
       
      }

    return (EXIT_SUCCESS);
}
